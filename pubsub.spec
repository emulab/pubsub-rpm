
Summary:	The Emulab publish/subscribe system.
Name:		pubsub
Version:	0.99.1
Release:	1%{?dist}
License:	LGPL
Group:		Applications/System
Source:		%{name}-%{version}.tar.bz2
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root
URL:		https://www.emulab.net
BuildRequires:  autoconf
BuildRequires:  libtool
BuildRequires:	make
BuildRequires:  byacc
BuildRequires:  openssl-devel
Requires:       %{name}-libs = %{version}-%{release}

%description
This software provides the Emulab publish-subscribe daemons
necessary to receive and send events across an Emulab cluster.

%package libs
Summary: The Emulab publish/subscribe system -- libraries.
Provides: libpubsub.so.1()(64bit)
Provides: libpubsub_r.so.1()(64bit)
%description libs
This software provides the Emulab publish-subscribe libraries
necessary to receive and send events across an Emulab cluster.

%package devel
Summary: Files for development of applications which use the Emulab publish/subscribe system.
%description devel
This software provides the Emulab publish-subscribe headers
and static libraries necessary to build publish-subscribe
applications.

%prep
%setup -n %{name}-%{version}

%build
%configure --enable-shared
make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf %{buildroot}

%files
%{_libexecdir}/*
/etc/init.d/pubsubd
/lib/systemd/system/pubsubd.service
%exclude /etc/rc*.d
%exclude /lib/systemd/system/multi-user.target.wants

%files devel
%{_libdir}/*.a
%{_libdir}/*.la
%{_includedir}/pubsub/*

%files libs
%{_libdir}/*.so*

%changelog
* Fri Jul 21 2023 David M. Johnson <johnsond@flux.utah.edu> - 0.99.1-1
- Minor systemd unit file fix.
* Fri Apr 07 2023 David M. Johnson <johnsond@flux.utah.edu> - 0.99-2
- Push/bump to production.
* Mon Jan 02 2022 David M. Johnson <johnsond@flux.utah.edu> - 0.99+20221209+a887a885-1
- CI testing.
* Sat Aug 20 2022 David M. Johnson <johnsond@flux.utah.edu> - 0.99-1
- Initial packaging.
